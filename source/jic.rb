require 'tk'
require 'tkextlib/tile'

class Employee
    def initialize(name, position, rate, days)
        @name = name
        @position = position
        @rate = rate
        @days = days
    end

    def get_name
        return @name
    end

    def get_position
        return @position
    end

    def get_rate
        return @rate
    end

    def get_days
        return @days
    end

    def amount
        return @rate*@days
    end

end

class Project
    def initialize(name,location, date, content,row)
        @name = name
        @members = []
        obj = self
        @label = Tk::Tile::Label.new(content) {text name}.grid( :column => 0, :row => row, :sticky => 'we', :pady => 5, :padx => 5);
        @edit = Tk::Tile::Button.new(content) {text "Edit Project"; command {obj.edit_project}}.grid( :column => 5, :row => row, :padx => 5, :pady => 5)
        @archive = Tk::Tile::Button.new(content) {text "Archive Project"; command {self.edit_project}}.grid( :column => 6, :row => row, :padx => 5, :pady => 5)
        for a in 1..3
            emp_name = "Employee " << String(a)
            emp = Employee.new(emp_name, 'Random', rand(100..1000), rand(1..20))
            @members.push(emp)
        end
    end

    def done_edit
        $edit.grid_remove
        $content.grid :sticky => 'nsew'
    end

    def add_tomem
        emp = Employee.new($empname.value, $emppos.value, Integer($emprate.value), Integer($empdays.value))
        @members.push(emp)
        for a in $addemp.winfo_children
            a.destroy
        end
        self.edit_project

    end

    def cancel_emp
        $addemp.grid_remove
        $edit.grid
    end

    def add_emp
        $edit.grid_remove
        $addemp.grid :sticky => 'nsew'
        $empname = TkVariable.new
        $emppos = TkVariable.new
        $emprate = TkVariable.new
        $empdays = TkVariable.new
        obj = self
        Tk::Tile::Label.new($addemp) {text 'Name'}.grid( :column => 0, :row => 0, :sticky => 'we', :pady => 5, :padx => 5);
        Tk::Tile::Entry.new($addemp) {width 7; textvariable $empname}.grid( :column => 1, :row => 0, :sticky => 'nsew', :padx => 5, :pady => 5)

        Tk::Tile::Label.new($addemp) {text 'Position'}.grid( :column => 0, :row => 1, :sticky => 'we', :pady => 5, :padx => 5);
        Tk::Tile::Entry.new($addemp) {width 7; textvariable $emppos}.grid( :column => 1, :row => 1, :sticky => 'nsew', :padx => 5, :pady => 5)

        Tk::Tile::Label.new($addemp) {text 'Rate'}.grid( :column => 0, :row => 2, :sticky => 'we', :pady => 5, :padx => 5);
        Tk::Tile::Entry.new($addemp) {width 7; textvariable $emprate}.grid( :column => 1, :row => 2, :sticky => 'nsew', :padx => 5, :pady => 5)

        Tk::Tile::Label.new($addemp) {text 'Days'}.grid( :column => 0, :row => 3, :sticky => 'we', :pady => 5, :padx => 5);
        Tk::Tile::Entry.new($addemp) {width 7; textvariable $empdays}.grid( :column => 1, :row => 3, :sticky => 'nsew', :padx => 5, :pady => 5)

        Tk::Tile::Button.new($addemp) {text "Add Employee"; command {obj.add_tomem}}.grid( :column => 0, :row => 8, :padx => 5, :pady => 10)
        cancelEmp = Tk::Tile::Button.new($addemp) {text "Cancel"; command {obj.cancel_emp}}.grid( :column => 1, :row => 8, :padx => 5, :pady => 10)
    end

    def edit_project
        $content.grid_remove
        $edit.grid :sticky => 'nsew'
        neym = TkVariable.new
        emp_name = TkVariable.new
        emp_posi = TkVariable.new
        neym.value = @name
        temp = []
        for a in @members
            temp.push(a.get_name)
        end
        mem_length = @members.length
        mems = TkVariable.new(temp)
        ctr = 0
        obj = self
        f = Tk::Tile::Frame.new($edit) {padding "3 3 3 3"; borderwidth 2; relief 'sunken'}.grid( :sticky => 'nsew', :column => 0, :row => 6, :pady => 20)
        l = TkListbox.new($edit){listvariable mems; height mem_length}.grid(:column => 1, :row => 6, :padx => 5, :pady => 20, :sticky => 'nswe')
        Tk::Tile::Label.new($edit) {text "Editing #{neym.value}"}.grid( :column => 0, :row => 0, :sticky => 'we', :pady => 5, :padx => 5);

        add_emp = Tk::Tile::Button.new($edit) {text "Add Employees"; command {obj.add_emp}}.grid( :column => 0, :row => 3, :padx => 5, :pady => 5)

        edit_emp = Tk::Tile::Button.new($edit) {text "Edit Employee's Info"; command {obj.add_emp}}.grid( :column => 1, :row => 3, :sticky => 'new', :padx => 5, :pady => 5)

        done = Tk::Tile::Button.new($edit) {text "Done Editing"; command {obj.done_edit}}.grid( :column => 2, :row => 3, :sticky => 'new', :padx => 5, :pady => 5)

        Tk::Tile::Label.new(f) {text 'Employees'}.grid( :column => 0, :row => ctr, :sticky => 'we', :pady => 5, :padx => 5);
        Tk::Tile::Label.new(f) {text 'Position'}.grid( :column => 1, :row => ctr, :sticky => 'we', :pady => 5, :padx => 5);
        Tk::Tile::Label.new(f) {text 'Rate'}.grid( :column => 2, :row => ctr, :sticky => 'we', :pady => 5, :padx => 5);
        Tk::Tile::Label.new(f) {text 'Days'}.grid( :column => 3, :row => ctr, :sticky => 'we', :pady => 5, :padx => 5);
        Tk::Tile::Label.new(f) {text 'Amount'}.grid( :column => 4, :row => ctr, :sticky => 'we', :pady => 5, :padx => 5);
        ctr = ctr + 1

        for a in @members
            Tk::Tile::Label.new(f) {text a.get_name}.grid( :column => 0, :row => ctr, :sticky => 'we', :pady => 5, :padx => 5);
            Tk::Tile::Label.new(f) {text a.get_position}.grid( :column => 1, :row => ctr, :sticky => 'we', :pady => 5, :padx => 5);
            Tk::Tile::Label.new(f) {text a.get_rate}.grid( :column => 2, :row => ctr, :sticky => 'we', :pady => 5, :padx => 5);
            Tk::Tile::Label.new(f) {text a.get_days}.grid( :column => 3, :row => ctr, :sticky => 'we', :pady => 5, :padx => 5);
            Tk::Tile::Label.new(f) {text a.amount}.grid( :column => 4, :row => ctr, :sticky => 'we', :pady => 5, :padx => 5);
            ctr = ctr + 1
        end
    end
end

root = TkRoot.new {title "JIC Payroll System"}
$login = Tk::Tile::Frame.new(root) {padding "3 3 3 3"}.grid( :sticky => 'nsew')
$content = Tk::Tile::Frame.new(root) {padding "3 3 3 3"}
$add = Tk::Tile::Frame.new(root) {padding "3 3 3 3"}
$addemp = Tk::Tile::Frame.new(root) {padding "3 3 3 3"}
$edit = Tk::Tile::Frame.new(root) {padding "3 3 3 3"}
#projects = Tk::Tile::Frame.new(root) {padding "3 3 3 3"} projects.grid :sticky => 'nsew'

$password = TkVariable.new
$projName = TkVariable.new
$projLoc = TkVariable.new
$projDate = TkVariable.new

logEntry = Tk::Tile::Entry.new($login) {width 7; textvariable $password; show '*'}.grid( :column => 0, :row => 1, :pady => 5, :columnspan => 3, :sticky => 'we' )
Tk::Tile::Label.new($login) {text "Enter password"}.grid( :column => 0, :row => 0, :sticky => 'we', :pady => 5);
Tk::Tile::Button.new($login) {text "Log-in"; command {logged_in($content, $login)}}.grid( :column => 0, :row => 2, :pady => 5)

addProj = Tk::Tile::Button.new($content) {text "Add Project"; command {add_project}}.grid( :column => 0, :row => 0, :padx => 5, :pady => 5)
viewArch = Tk::Tile::Button.new($content) {text "View Archives"; command {view_archives}}.grid( :column => 1, :row => 0, :padx => 5, :pady => 5)


$cnt = 5
for a in 1..3
    projName = "Project " << String(a)
    proj = Project.new(projName, 'Somewhere', 'Somedate', $content,$cnt)
    $cnt = $cnt + 1
end

def logged_in(content, login)
    login.destroy
    content.grid :sticky => 'nsew'
end

def add_project
    $content.grid_remove
    $add.grid :sticky => 'nsew'

    Tk::Tile::Label.new($add) {text "Enter project name:"}.grid( :column => 0, :row => 3, :sticky => 'we', :padx => 5, :pady => 5);
    name = Tk::Tile::Entry.new($add) {width 7; textvariable $projName}.grid( :column => 1, :row => 3, :sticky => 'nsew', :padx => 5, :pady => 5, :columnspan => 4 )

    Tk::Tile::Label.new($add) {text "Enter project location:"}.grid( :column => 0, :row => 5, :sticky => 'we', :padx => 5, :pady => 5);
    location = Tk::Tile::Entry.new($add) {width 7; textvariable $projLoc}.grid( :column => 1, :row => 5, :sticky => 'new', :padx => 5, :pady => 5, :columnspan => 10 )

    Tk::Tile::Label.new($add) {text "Enter when project started:"}.grid( :column => 0, :row => 7, :sticky => 'we', :padx => 5, :pady => 5);
    date = Tk::Tile::Entry.new($add) {width 7; textvariable $projdate}.grid( :column => 1, :row => 7, :sticky => 'new', :padx => 5, :pady => 5, :columnspan => 10 )

    addProj = Tk::Tile::Button.new($add) {text "Add Project"; command {add_proj}}.grid( :column => 0, :row => 8, :padx => 5, :pady => 10)
    cancelProj = Tk::Tile::Button.new($add) {text "Cancel"; command {cancel_proj}}.grid( :column => 1, :row => 8, :padx => 5, :pady => 10)
end

def edit_proj(proj)
    proj.edit_project
end

def add_proj
    proj = Project.new($projName.value, $projLoc, $projDate, $content,$cnt)
    $cnt = $cnt + 1
    for a in $add.winfo_children
        a.destroy
    end
    $add.grid_remove
    edit_proj(proj)
end

def cancel_proj
    $add.grid_remove
    $content.grid
end

Tk.mainloop
