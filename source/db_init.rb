
# NOTE!!
# Running this code creates a new jic.sqlite file.
# If one already exists, it will be deleted.
# Use only when setting up (or resetting) the system.

require 'rubygems'
require 'sqlite3'

DBNAME = "jic.sqlite"
File.delete(DBNAME) if File.exists?DBNAME

DB = SQLite3::Database.new( DBNAME )
# Projects table:
DB.execute("CREATE TABLE Projects(id, title, client, date_started, date_finished)")
# Sample Project:
#insert_query = "INSERT INTO Projects VALUES (1, 'JIC Bldg.', 'Ivan', 'today', NULL)"
#DB.execute(insert_query)
# Test query:
result = DB.execute("SELECT * FROM Projects")
puts "Projects: "
print result
puts

# Workers table:
DB.execute("CREATE TABLE Workers(id, lname, fname, position, rate, philhealth, adjustments, vale, days, current_salary, overtime_rate, undertime_rate, overtime_hours, undertme_hours)")
# Sample Worker:
#insert_query = "INSERT INTO Workers VALUES (1, 'dela Cruz', 'Juan', 'Mason', 450.5, 0, 0, 0, 0, 0, 0, 0, 0, 0)"
#DB.execute(insert_query)
# Test query:
result = DB.execute("SELECT * FROM Workers")
puts "\nWorkers: "
print result
puts

# Relationship table:
DB.execute("CREATE TABLE Relate(project_id, worker_id, UNIQUE(project_id, worker_id))")
# Sample Relationship:
#insert_query = "INSERT INTO Relate VALUES (1, 1)"
#DB.execute(insert_query)
# Test query:
result = DB.execute("SELECT * FROM Relate")
puts "\nRelationships: "
print result
puts