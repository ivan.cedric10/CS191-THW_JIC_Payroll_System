require 'tk'
require './admin'
require './worker'
require './project'
require 'tkextlib/tile'

$test=0

class View
    attr_accessor :root, :projects, :employees

    def initialize(root, admin)
        @root=root
        @projects=admin.projects
        @employees=admin.workers
        @admin=admin
        main()
    end

    def main
        a=Ttk::TNotebook.new(@root) do
            height 300
            width 450
            grid(:row => 0, :column => 0, :sticky => 'nsew')
        end

        tree = Ttk::Treeview.new(a) do
            columns 'id name position rate'
            show 'headings'
            selectmode 'browse'
            grid(:row => 0, :column => 0, :sticky => 'nsew')
        end

        scrolly = Ttk::Scrollbar.new(tree) do
            orient 'vertical'
            grid(:row => 0, :column => 1, :sticky => 'nse')
        end

        scrollx = Ttk::Scrollbar.new(tree) do
            orient 'horizontal'
            grid(:row => 1, :column => 0, :sticky => 'ews')
        end

        TkGrid.columnconfigure(tree, 0, :weight => 1)
        TkGrid.rowconfigure(tree, 0, :weight => 1)

        tree.configure(:yscrollcommand => proc{|*args| scrolly.set(*args)})
        tree.configure(:xscrollcommand => proc{|*args| scrollx.set(*args)})

        scrolly.configure(:command => proc{|*args| tree.yview(*args)})
        scrollx.configure(:command => proc{|*args| tree.xview(*args)})

        tree.column_configure('id', :width => 30)
        tree.column_configure('name', :width => 180)
        tree.column_configure('position', :width => 90)
        tree.column_configure('rate', :width => 60)
        tree.heading_configure('id', :text => 'id')
        tree.heading_configure('name', :text => 'name')
        tree.heading_configure('position', :text => 'position')
        tree.heading_configure('rate', :text => 'rate')

        a.add(tree, :text => 'Summary')

        @employees.each do |emp|
            tree.insert('', 'end', :values => [emp.id, "#{emp.last_name}, #{emp.first_name}", emp.position, emp.rate])
        end

        @projects.each do |proj|
            tree = Ttk::Treeview.new(a) do
                columns 'id name position rate'
                show 'headings'
                selectmode 'browse'
                grid(:row => 0, :column => 0, :sticky => 'nsew')
            end

            tree.column_configure('id', :width => 30)
            tree.column_configure('name', :width => 180)
            tree.column_configure('position', :width => 90)
            tree.column_configure('rate', :width => 60)
            tree.heading_configure('id', :text => 'id')
            tree.heading_configure('name', :text => 'name')
            tree.heading_configure('position', :text => 'position')
            tree.heading_configure('rate', :text => 'rate')

            a.add(tree, :text => proj.title)

            proj.workers.each do |emp|
                tree.insert('', 'end', :values => [emp.id, "#{emp.last_name}, #{emp.first_name}", emp.position, emp.rate])
            end
        end

        $var1 = TkVariable.new
        $var2 = TkVariable.new
        $var3 = TkVariable.new
        $var4 = TkVariable.new

        f1 = Ttk::TFrame.new(a)

        pe = Ttk::TEntry.new(f1) do
            textvariable $var1
            grid(:row => 0, :column => 0, :pady => 0, :sticky => 's')
        end

        ap = Ttk::TButton.new(f1) do
            text 'Add Project'
            grid(:row => 1, :column => 0, :ipadx => 20, :ipady => 10, :sticky => 'n')
        end

        ap.bind('1') {add_proj(a)}

        a.add(f1, :text => '+')

        @edit = Ttk::TFrame.new(@root)

        Ttk::TLabel.new(@edit) do
            text 'Name: '
            grid(:row => 0, :column => 0)
        end

        Ttk::TEntry.new(@edit) do
            textvariable $var2
            grid(:row => 0, :column => 1)
        end

        Ttk::TLabel.new(@edit) do
            text 'Rate'
            grid(:row => 1, :column => 0)
        end

        Ttk::TEntry.new(@edit) do
            textvariable $var3
            grid(:row => 1, :column => 1, :pady => 5)
        end

        Ttk::TLabel.new(@edit) do
            text 'position'
            grid(:row => 0, :column => 2, :sticky => 'e')
        end

        Ttk::TCombobox.new(@edit) do
            textvariable $var4
            values 'Mason Driver Helper Foreman Office'
            state 'readonly'
            grid(:row => 0, :column => 3, :sticky => 'e')
        end

        @ok = Ttk::TButton.new(@edit) do
            grid(:row => 1, :column => 2, :sticky => 'se')
        end

        temp = Ttk::TButton.new(@edit) do
            text 'Exit'
            grid(:row => 1, :column => 3, :sticky => 'se')
        end
        temp.bind('1') {@edit.grid_remove}

        @add = Ttk::TFrame.new(@root)

        $emplist=TkVariable.new(@employees)

        @list = Tk::Listbox.new(@add) do
            listvariable $emplist
            height 5
            grid(:row => 0, :column => 0, :rowspan => 3)
        end

        @okadd = Ttk::TButton.new(@add) do
            text 'Add Worker'
            grid(:row => 0, :column => 1)
        end
        @okadd.bind('1') {add_exist(a.selected)}

        temp = Ttk::TButton.new(@add) do
            text 'New Worker'
            grid(:row => 1, :column => 1)
        end
        temp.bind('1') {add_worker(a.selected)}

        temp = Ttk::TButton.new(@add) do
            text 'Cancel'
            grid(:row => 2, :column => 1)
        end
        temp.bind('1') {@add.grid_remove}

        buttons = Ttk::TFrame.new(@root) do
            grid(:row => 1, :column => 0, :sticky => 'ew')
        end

        temp = Ttk::TButton.new(buttons) do
            text 'Add Worker'
            grid(:row => 2, :column => 0, :sticky => 'w')
        end
        temp.bind('1') {add_worker(a.selected)}

        temp = Ttk::TButton.new(buttons) do
            text 'Edit Worker'
            grid(:row => 2, :column => 1, :sticky => 's')
        end
        temp.bind('1') {edit_worker(a.selected)}

        temp = Ttk::TButton.new(buttons) do
            text 'Delete Worker'
            grid(:row => 2, :column => 2, :sticky => 'e')
        end
        temp.bind('1') {del_worker(a.selected)}

        TkGrid.columnconfigure(@root, 0, :weight => 1)
        TkGrid.columnconfigure(f1, 0, :weight => 1)
        TkGrid.columnconfigure(@edit, 2, :weight => 1)
        TkGrid.columnconfigure(@edit, 3, :weight => 1)
        TkGrid.columnconfigure(buttons, 0, :weight => 1)
        TkGrid.columnconfigure(buttons, 1, :weight => 1)
        TkGrid.columnconfigure(buttons, 2, :weight => 1)
        TkGrid.rowconfigure(@root, 0, :weight => 1)
        TkGrid.rowconfigure(f1, 0, :weight => 1)
        TkGrid.rowconfigure(f1, 1, :weight => 1)
    end

    def add_exist(tree)
        work = @employees[@list.curselection[0]]
        print work
        tree.insert('','end', :values => [work.id, work.last_name+work.first_name, work.position, "#{work.rate}"])
        @projects[tree.winfo_parent.index(tree)-1].workers.push(work)
        @add.grid_remove
    end

    def add_proj(note)
        tree = Ttk::Treeview.new(note) do
            columns 'id name position rate'
            show 'headings'
            selectmode 'browse'
            grid(:row => 0, :column => 0, :sticky => 'nsew')
        end

        tree.column_configure('id', :width => 30)
        tree.column_configure('name', :width => 180)
        tree.column_configure('position', :width => 90)
        tree.column_configure('rate', :width => 60)
        tree.heading_configure('id', :text => 'id')
        tree.heading_configure('name', :text => 'name')
        tree.heading_configure('position', :text => 'position')
        tree.heading_configure('rate', :text => 'rate')

        @projects.push(Project.new)

        note.insert(note.tabs.length-1, tree, :text => $var1.value)
        note.select(tree)
        $var1.value = '' 
    end

    def add_worker(tree)
        @add.grid_remove
        if tree.winfo_parent.index(tree)==0 or $test==1
            @ok.configure(:text => 'Add')
            @edit.grid(:row => 2, :column => 0, :sticky => 'nsew')
            @ok.bind('ButtonRelease-1') {add(tree)}
            $test=0

            $var2.value=''
            $var3.value=''
            $var4.value=''
        else
            @add.grid(:row => 2, :column => 0, :sticky => 'nsew')
            $test=1
        end
    end

    def add(tree)
        work=Worker.new(@employees.length+1, "#{$var2.value.split[1]}", "#{$var2.value.split[0]}", $var4.value, $var3.value.to_i)
        tree.insert('','end', :values => [@employees.length+1, $var2.value, $var4.value, $var3.value])
        @employees.push(work)
        puts @employees
        $emplist.value=@employees
        @edit.grid_remove
    end

    def del_worker(tree)
        @edit.grid_remove
        if(tree.selection!=[])
            tree.delete(tree.selection[0])
        end
    end

    def edit_worker(tree)
        if(tree.selection!=[])
            @ok.configure(:text => 'Edit')
            @edit.grid(:row => 2, :column => 0, :sticky => 'nsew')
            $var2.value = "#{tree.get(tree.selection[0],1).split(', ')[1]} #{tree.get(tree.selection[0],1).split(', ')[0]}"
            $var3.value = tree.get(tree.selection[0],3)
            $var4.value = tree.get(tree.selection[0],2)
            item = tree.selection[0]
            @ok.bind('ButtonRelease-1') {item.configure(:values => [tree.get(item,0), "#{$var2.value.split[1]}, #{$var2.value.split[0]}", $var4.value, $var3.value])}
        end
    end
end

a=TkRoot.new
admin = Admin.new
admin.load("jic.sqlite")
b=View.new(a,admin)
a.mainloop()
admin.save("jic.sqlite")

