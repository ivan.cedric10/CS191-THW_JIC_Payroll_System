# Note: install Shoes to run
#		go to terminal and type 'shoes sh.rb'

Shoes.app :width => 600, :height => 600, title: "JIC Payroll" do
	#admin_pass = ask("Enter admin password", :title => "Enter password")
	main_stack = stack
	main_stack.append do
		f1 = flow
		f1.append do
			@b1 = button("Add Project", height: 100, width: 195, margin: 10)
			@b2 = button("View Archives", height: 100, width: 195, margin: 10)

			@b1.click{
=begin
				main_stack.toggle
				add_stack = stack
				add_stack.append do
					flow margin: 10 do
						para "Project Name:"
						new_proj = edit_line margin_left: 10
					end
					flow margin: 10 do
						para "Location:"
						loc = edit_line margin_left: 10
					end
					flow margin: 10 do
						para "Date Started:"
						date_start = edit_line margin_left: 10
					end

					add = button("Add Project")
					add.click{

						add_stack.toggle
						main_stack.toggle
					}
				end
=end
				main_stack.app do

					flc = flow
						flc.append do
							para new_project.text
							buttonEdit = button("Edit Project", right: 175)
							buttonArchive = button("Archive Project", right: 0)

							buttonEdit.click{
								window title: "Edit Project: #{new_project}" do
									para "Edit Project: #{buttonEdit.style[]}"
								end
							}

							buttonArchive.click{
								flc.remove
							}
						end
					end
			}
			@b2.click{
				window title: "View Archived Projects"
			}
		end
	end
end
