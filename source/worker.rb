class Worker
  attr_accessor :last_name, :first_name, :position, :rate, :id, :current_salary, :philhealth, :adjustments, :vale, :overtime_hours, :undertime_hours,
				:days, :overtime_rate, :undertime_rate

  def initialize(id=0, lname="", fname="", position="", rate=0, philhealth=0,
					adjustments=0, vale=0, days=0, curr=0, overtime_rate=0, undertime_rate=0,
					overtime_hours=0, undertime_hours=0)
    @id = id
    @last_name = lname
    @first_name = fname
    @position = position  # mason, helper, kargador, etc.
    @rate = rate  # pay per hour
	@days = days
	@philhealth = philhealth
	@adjustments = adjustments	
	@vale = vale
	@current_salary = curr	# base
	@overtime_rate = overtime_rate
	@undertime_rate = undertime_rate
	@overtime_hours = overtime_hours
	@undertime_hours = undertime_hours
    # etc.
  end
  
  def overtime_pay()
    return (self.rate / 8) * self.overtime_hours
  end
  
  def undertime()
    return (self.rate / 8) * self.undertime_hours
  end
  
  def edit_factors(philhealth=self.philhealth, adjustments=self.adjustments, vale=self.vale,
                   overtime_rate=self.overtime_rate, undertime_rate=self.undertime_rate)
    @philhealth = philhealth
	@adjustments = adjustments
	@vale = vale
	@overtime_rate = overtime_rate
	@undertime_rate = undertime_rate
  end
  
  def total()
    total = self.current_salary + self.adjustments + self.overtime_pay() - self.undertime() - self.vale
	return total
  end
  
  def to_s
    return "#{@id} #{@last_name}, #{@first_name} -- #{@position}"
  end
end
