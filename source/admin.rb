
# Change log:
#  03/10/16 added Delete worker and Delete project
#  04/19/16 working database

require 'rubygems'
require 'sqlite3'
require_relative 'project'
require_relative 'worker'
require_relative 'output'
#require_relative "db_init"

class Admin
  attr_accessor :projects, :workers
  
  def initialize
    @authentication = "iamroot"
    @authenticated = true
    @projects = []    
    @workers = []    
  end
  
  def reset_db()
    require_relative 'db_init'
  end
  
  def load(db_filename)
	db = SQLite3::Database.new( db_filename )
	query = "SELECT * FROM Projects;"
	raw_proj = db.execute(query)
	raw_proj.each do |raw|
	  self.create_project(raw[0], raw[1], raw[2], raw[3], raw[4])
	end
	query = "SELECT * FROM Workers;"
	raw_work = db.execute(query)
	raw_work.each do |raw|
	  self.add_worker(raw[0], raw[1], raw[2], raw[3], raw[4], raw[5], raw[6], raw[7],
						raw[8], raw[9], raw[10], raw[11], raw[12], raw[13])
	end
	
	query = "SELECT * FROM Relate"
	raw_rel = db.execute(query)
	raw_rel.each do |raw|
	  employ(@workers[raw[1]-1], @projects[raw[0]-1])
	end
  end
  
  def save(db_filename)
    #File.delete(db_filename) if File.exists?db_filename
    db = SQLite3::Database.new( db_filename )
	db.execute("DELETE FROM Projects")
	db.execute("DELETE FROM Workers")
	db.execute("DELETE FROM Relate")

	#max = db.execute("SELECT COUNT(1) FROM Workers;")
	@workers.each do |w|
	  #if w.id > max[0][0]
        insert = "INSERT INTO Workers VALUES (#{w.id}, '#{w.last_name}', '#{w.first_name}', '#{w.position}', #{w.rate}, #{w.days}, #{w.philhealth},
					#{w.adjustments}, #{w.vale}, #{w.current_salary}, #{w.overtime_rate}, #{w.undertime_rate}, #{w.overtime_hours}, #{w.undertime_hours})"
		db.execute(insert)
	  #end
	end
	
	#max = db.execute("SELECT COUNT(1) FROM Projects;")
	@projects.each do |p|
	 # if p.id > max[0][0]
        insert = "INSERT INTO Projects VALUES (#{p.id}, '#{p.title}', '#{p.client}', '#{p.date_started}', '#{p.date_finished}')"
		db.execute(insert)
	  #end
	end
	
	@projects.each do |p|
	  p.workers.each do |w|
	    db.execute("INSERT OR IGNORE INTO Relate VALUES (#{p.id}, #{w.id})")
	  end
	end
	puts db.execute("SELECT * FROM Projects")
  end

  def create_project(id, title, client, date_started, date_finished)
    new_project = Project.new(id, title, [], client, "SELECT date(now)", date_finished)
    @projects.push(new_project)
    return new_project
  end
  
  def add_worker(id, lname, fname, position, rate, philhealth, adjustments, vale, days, current_salary, overtime_rate, undertime_rate, overtime_hours, undertme_hours) # adds worker to admin database (no project yet)
    new_worker = Worker.new(id, lname, fname, position, rate, philhealth, adjustments, vale, days, current_salary, overtime_rate, undertime_rate, overtime_hours, undertme_hours)
    @workers.push(new_worker)
    return new_worker
  end
  
  def employ(worker, project)  # this method places a worker instance to a project instance
    if @projects.include? project and @workers.include? worker
      unless project.workers.include? worker
        project.workers.push(worker)
      end
    end
  end
  
  def del_worker(worker, project) # delete worker from project
    # archive code here
    project.workers.delete(worker)
  end
  
  def remove_worker(worker) # archive worker (remove from everything)
    # archive code here
    for p in @projects
		p.workers.delete(worker)
	end
	    @workers.delete(worker)
  end
  
  def del_project(project)
    #if project.class.name != "Project"
	#  return
	#end
	@projects.delete_at(project)
	puts @projects
    #@projects.delete(project)
  end
  
  def payroll
	pdf = Pdf.new
	pdf.print_file(@workers)
  end
  
end

admin = Admin.new
admin.load("jic.sqlite")
#admin.save("jic.sqlite")
#admin.payroll
#puts Dir.pwd
#puts admin.projects
#puts
#puts admin.workers

#proj = a.create_project("titty")
#puts b.to_str
#d = a.create_project("tittie")
#puts d.to_str

#worker = a.add_worker("dela Cruz", "John")
#puts e.to_str
#f = a.add_worker("Doe", "Juan")
#puts f.to_str

#admin.employ(worker,proj)
#a.employ(f,b)
#puts proj.workers

#admin.del_project(proj)
#puts admin.projects
