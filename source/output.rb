require "prawn"

class Pdf
	def initialize
		
	end
	def print_file(worker_list)
		Prawn::Document.generate("hello.pdf") do
			#worker_list = ['a','b','c','d','e','f','g','h','i']
			position = [[0, 750], [300, 750], [0, 480], [300, 480], [0, 220], [300, 220]]
			pos = 0
			worker_list.each do |worker|
				bounding_box(position[pos%6], :width => 250, :height => 220) do
					pad(20) { text "REM A DEO Construction", :align => :center, :style => :bold }
					formatted_text [{ :text => "Name:        ", :styles => [:bold] },
									{ :text => "#{worker.last_name}, #{worker.first_name}" } ]
					formatted_text [{ :text => "Position:    ", :styles => [:bold] },
									{ :text => "#{worker.position}" } ]
					formatted_text [{ :text => "Days:        ", :styles => [:bold] },
									{ :text => "#{worker.days}     Php #{worker.days * worker.rate}" } ]
					formatted_text [{ :text => "OT:           ", :styles => [:bold] },
									{ :text => "#{worker.overtime_hours}    Php #{worker.overtime_hours * worker.overtime_rate}" } ]
					formatted_text [{ :text => "Gross:       ", :styles => [:bold] },
									{ :text => "#{worker.current_salary}" } ]
					move_down 15
					text "Deductions", :style => :bold
					pad(20) { formatted_text [{ :text => "Net: Php #{worker.total}", :styles => [:bold, :underline]}] }
					transparent(0.5) { dash(5); stroke_bounds; undash }
				end
				pos+=1
				if pos % 6 == 0
					start_new_page
				end
			end
		end
		output = `start "" /max "hello.pdf"`
	end
end

#pdf = Pdf.new