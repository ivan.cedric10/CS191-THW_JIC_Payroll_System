require 'tk'
require './admin'
require './worker'
require './project'
require 'tkextlib/tile'

$test=0

class View
    attr_accessor :root, :projects, :employees, :admin

    def initialize(root, admin)
        @root=root
        #admin.projects=admin.projects
        #admin.workers=admin.workers
        @admin=admin
        @root.state("zoomed")
        main()
    end

    def main
        TkOption.add 'tearoff', 0

####### Initialize
        a=Ttk::TNotebook.new(@root) do
            height 300
            width 450
            grid(:row => 0, :column => 0, :sticky => 'nsew')
        end

        menubar=TkMenu.new(@root)
        menubar.add('command', 'label' => "Add Project", 'command' => proc{add_proj_win(a)})
        menubar.add('command', 'label' => "Print Payroll", 'command' => proc{admin.payroll})
		menubar.add('command', 'label' => "Delete Project", 'command' => proc{del_proj(a)})#, 'command' => proc{admin.payroll})
        @root.menu(menubar)

        tree = Ttk::Treeview.new(a) do
            columns 'id name position rate'
            show 'headings'
            selectmode 'browse'
            grid(:row => 0, :column => 0, :sticky => 'nsew')
        end

        scrolly = Ttk::Scrollbar.new(tree) do
            orient 'vertical'
            grid(:row => 0, :column => 1, :sticky => 'nse')
        end

        scrollx = Ttk::Scrollbar.new(tree) do
            orient 'horizontal'
            grid(:row => 1, :column => 0, :sticky => 'ews')
        end

        TkGrid.columnconfigure(tree, 0, :weight => 1)
        TkGrid.rowconfigure(tree, 0, :weight => 1)

        tree.configure(:yscrollcommand => proc{|*args| scrolly.set(*args)})
        tree.configure(:xscrollcommand => proc{|*args| scrollx.set(*args)})

        scrolly.configure(:command => proc{|*args| tree.yview(*args)})
        #scrollx.configure(:command => proc{|*args| tree.xview(*args)})

        tree.column_configure('id', :width => 30, :anchor => 'center')
        tree.column_configure('name', :width => 180, :anchor => 'center')
        tree.column_configure('position', :width => 90, :anchor => 'center')
        tree.column_configure('rate', :width => 60, :anchor => 'center')
		
        tree.heading_configure('id', :text => 'ID')
        tree.heading_configure('name', :text => 'Name')
        tree.heading_configure('position', :text => 'Position')
        tree.heading_configure('rate', :text => 'Rate')

        a.add(tree, :text => 'Summary')

        admin.workers.each do |emp|
            tree.insert('', 'end', :values => [emp.id, "#{emp.last_name}, #{emp.first_name}", emp.position, emp.rate])
        end

        admin.projects.each do |proj|
            tree = Ttk::Treeview.new(a) do
                columns 'id name position rate salary days overtime_h undertime_h'
                show 'headings'
                selectmode 'browse'
                grid(:row => 0, :column => 0, :sticky => 'nsew')
            end

            tree.column_configure('id', :width => 30, :anchor => 'center')
            tree.column_configure('name', :width => 180, :anchor => 'center')
            tree.column_configure('position', :width => 90, :anchor => 'center')
            tree.column_configure('rate', :width => 60, :anchor => 'center')
			tree.column_configure('salary', :width => 60, :anchor => 'center')
			tree.column_configure('days', :width => 60, :anchor => 'center')
			tree.column_configure('overtime_h', :width => 60, :anchor => 'center')
			tree.column_configure('undertime_h', :width => 60, :anchor => 'center')
            tree.heading_configure('id', :text => 'ID')
            tree.heading_configure('name', :text => 'Name')
            tree.heading_configure('position', :text => 'Position')
            tree.heading_configure('rate', :text => 'Rate')
			tree.heading_configure('salary', :text => 'Salary')
			tree.heading_configure('days', :text => 'Days Present')
			tree.heading_configure('overtime_h', :text => 'Overtime Hours')
			tree.heading_configure('undertime_h', :text => 'Undertime Hours')

            a.add(tree, :text => proj.title)

            proj.workers.each do |emp|
                tree.insert('', 'end', :values => [emp.id, "#{emp.last_name}, #{emp.first_name}", emp.position, emp.rate, emp.current_salary, emp.days, emp.overtime_hours, emp.undertime_hours])
            end
        end

        $var1 = TkVariable.new
        $var2 = TkVariable.new
        $var3 = TkVariable.new
        $var4 = TkVariable.new
		$var5 = TkVariable.new
        $emplist=TkVariable.new(admin.workers)

        @add = Ttk::TFrame.new(@root)

        @list = Tk::Listbox.new(@add) do
            listvariable $emplist
            height 5
            grid(:row => 0, :column => 0, :rowspan => 3)
        end

        @okadd = Ttk::TButton.new(@add) do
            text 'Add Worker'
            grid(:row => 0, :column => 1)
        end
        @okadd.bind('1') {add_exist(a.selected)}

        temp = Ttk::TButton.new(@add) do
            text 'New Worker'
            grid(:row => 1, :column => 1)
        end
        temp.bind('1') {add_worker(a.selected)}

        temp = Ttk::TButton.new(@add) do
            text 'Cancel'
            grid(:row => 2, :column => 1)
        end
        temp.bind('1') {@add.grid_remove}

        buttons = Ttk::TFrame.new(@root) do
            grid(:row => 1, :column => 0, :sticky => 'ew')
        end

        Tk::Tile::Style.configure('Main.TButton', {"font" => "Arial 28", "borderwidth" => 10, "padding" => 5})

        temp = Ttk::TButton.new(buttons) do
            text 'Add Worker'
            grid(:row => 2, :column => 0, :sticky => 'w')
            style 'Main.TButton'
        end
        temp.bind('1') {add_worker(a.selected)}

        temp = Ttk::TButton.new(buttons) do
            text 'Edit Worker'
            grid(:row => 2, :column => 1, :sticky => 's')
            style 'Main.TButton'
        end
        temp.bind('1') {edit_worker(a, a.selected)}

        temp = Ttk::TButton.new(buttons) do
            text 'Delete Worker'
            grid(:row => 2, :column => 2, :sticky => 'e')
            style 'Main.TButton'
        end
        temp.bind('1') {del_worker(a.selected)}



        TkGrid.columnconfigure(@root, 0, :weight => 1)
    #    TkGrid.columnconfigure(@edit, 2, :weight => 1)
    #    TkGrid.columnconfigure(@edit, 3, :weight => 1)
        TkGrid.columnconfigure(buttons, 0, :weight => 1)
        TkGrid.columnconfigure(buttons, 1, :weight => 1)
        TkGrid.columnconfigure(buttons, 2, :weight => 1)
        TkGrid.rowconfigure(@root, 0, :weight => 1)
    end

    def add_exist(tree)
        @list.curselection.each do |i|
            work = admin.workers[i]
            tree.insert('','end', :values => [work.id, work.last_name+work.first_name, work.position, "#{work.rate}"])
            admin.projects[tree.winfo_parent.index(tree)-1].workers.push(work)
            @add.grid_remove
        end
    end

    def add_proj(note, employees)
        tree = Ttk::Treeview.new(note) do
            columns 'id name position rate salary days overtime_h undertime_h'
            show 'headings'
            selectmode 'browse'
            grid(:row => 0, :column => 0, :sticky => 'nsew')
        end

        tree.column_configure('id', :width => 30, :anchor => 'center')
        tree.column_configure('name', :width => 180, :anchor => 'center')
        tree.column_configure('position', :width => 90, :anchor => 'center')
        tree.column_configure('rate', :width => 60, :anchor => 'center')
		tree.column_configure('salary', :width => 60, :anchor => 'center')
		tree.column_configure('days', :width => 60, :anchor => 'center')
        tree.column_configure('overtime_h', :width => 60, :anchor => 'center')
		tree.column_configure('undertime_h', :width => 60, :anchor => 'center')
        tree.heading_configure('id', :text => 'ID')
        tree.heading_configure('name', :text => 'Name')
        tree.heading_configure('position', :text => 'Position')
        tree.heading_configure('rate', :text => 'Rate')
		tree.heading_configure('salary', :text => 'Salary')
		tree.heading_configure('days', :text => 'Days Present')
		tree.heading_configure('overtime_h', :text => 'Overtime Hours')
		tree.heading_configure('undertime_h', :text => 'Undertime Hours')


        
        
        if not employees.curselection.empty?
			new_proj=Project.new(id=note.index('end'), title=$var1.value, workers=admin.workers[employees.curselection[0]..employees.curselection[-1]])
            new_proj.workers.each do |emp|
                tree.insert('','end', :values => [emp.id, emp.last_name+emp.first_name, emp.position, "#{emp.rate}", "#{emp.rate*emp.days}", emp.days])
            end
		else
			new_proj=Project.new(id=note.index('end'), title=$var1.value)
        end
		admin.projects.push(new_proj)

        note.add(tree, :text => $var1.value)
        note.select(tree)

        $var1.value = ''
    end

    def add_proj_win(note)
        @root['state']='zoomed'
        $top=TkToplevel.new(@root)
        $top.wm_title('Add Project')

        Ttk::TLabel.new($top) do
            text 'Proj. Name: '
            grid(:row => 0, :column => 0, :sticky => 'nsew')
        end

        Ttk::TEntry.new($top) do
            textvariable $var1
            grid(:row => 0, :column => 1, :sticky => 'nsew')
        end

        Ttk::TLabel.new($top) do
            text 'Employees:'
            grid(:row => 1, :column => 0, :sticky => 'nsw', :columnspan => 2)
        end

        emp=Tk::Listbox.new($top) do
            listvariable $emplist
            height 7
            grid(:row => 2, :column => 0, :sticky => 'nsw', :columnspan => 2)
        end

        add = Ttk::TButton.new($top) do
            text 'Add'
            grid(:row => 3, :column => 0, :sticky => 'nsew')
            command "$top.destroy"
        end

        Ttk::TButton.new($top) do
            text 'Exit'
            grid(:row => 3, :column => 1, :sticky => 'nsew')
            command "$top.destroy"
        end

        add.bind('1'){add_proj(note, emp)}
    end
	
	def del_proj(note)
	  #if(note.selected!=[])
			puts note.index(note.selected).to_i-1
			admin.del_project(note.index(note.selected).to_i-1)
			#admin.projects.delete(admin.projects[note.index(note.selected).to_i])
			note.forget(note.selected)
        #end
	end

    def add_worker(tree)
        @add.grid_remove
        if tree.winfo_parent.index(tree)==0 or $test==1
            add_worker_win(tree)
        else
            @add.grid(:row => 2, :column => 0, :sticky => 'nsew')
            $test=1
        end
    end

    def add(tree)
        work=Worker.new(admin.workers.length+1, "#{$var2.value.split[1]}", "#{$var2.value.split[0]}", $var4.value, $var3.value.to_i)
        tree.insert('','end', :values => [admin.workers.length+1, $var2.value, $var4.value, $var3.value])
        admin.workers.push(work)
        $emplist.value=admin.workers
        $var1.value = ''
    end

    def add_worker_win(tree)
        $add_worker_window = TkToplevel.new
        @details = Ttk::TFrame.new($add_worker_window)
        @details.grid(:row => 0, :column => 0)
        $add_worker_window.wm_title("Add Worker")

        Ttk::TLabel.new(@details) do
            text 'Name: '
            grid(:row => 0, :column => 0, :sticky => 'nsew')
        end

        Ttk::TEntry.new(@details) do
            textvariable $var2
            grid(:row => 0, :column => 1, :sticky => 'nsew')
        end

        Ttk::TLabel.new(@details) do
            text 'Rate: '
            grid(:row => 1, :column => 0, :sticky => 'nsew')
        end

        Ttk::TEntry.new(@details) do
            textvariable $var3
            grid(:row => 1, :column => 1, :pady => 5)
        end

        Ttk::TLabel.new(@details) do
            text 'Position: '
            grid(:row => 2, :column => 0, :sticky => 'nsew')
        end

        Ttk::TCombobox.new(@details) do
            textvariable $var4
            values 'Mason Driver Helper Foreman Office'
            state 'readonly'
            grid(:row => 2, :column => 1, :sticky => 'nsew')
        end

        @buttonsframe = Ttk::TFrame.new($add_worker_window)
        @buttonsframe.grid(:row => 3, :column => 0)

        @ok = Ttk::TButton.new(@buttonsframe) do
            grid(:row => 0, :column => 0, :sticky => 'nsew')
            command "$add_worker_window.destroy"
        end

        temp = Ttk::TButton.new(@buttonsframe) do
            text 'Exit'
            grid(:row => 0, :column => 1, :sticky => 'nsew')
            command "$add_worker_window.destroy"
        end

        @ok.configure(:text => 'Add')
        @ok.bind('Button-1') {add(tree)}
    end

    def del_worker(tree)
        if(tree.selection!=[])
            tree.delete(tree.selection[0])
			admin.remove_worker(admin.workers[tree.get(tree.selection[0],0).to_i-1])
        end
    end

    def edit_worker(note, tree)
        if(tree.selection!=[] and note.index(note.selected).to_i != 0)
            $add_worker_window = TkToplevel.new
            @details = Ttk::TFrame.new($add_worker_window)
            @details.grid(:row => 0, :column => 0)
            $add_worker_window.wm_title("Edit Worker")

            Ttk::TLabel.new(@details) do
                text 'Name: '
                grid(:row => 0, :column => 0, :sticky => 'nsew')
            end

            Ttk::TEntry.new(@details) do
                textvariable $var2
                grid(:row => 0, :column => 1, :sticky => 'nsew')
            end

            Ttk::TLabel.new(@details) do
                text 'Rate: '
                grid(:row => 1, :column => 0, :sticky => 'nsew')
            end

            Ttk::TEntry.new(@details) do
                textvariable $var3
                grid(:row => 1, :column => 1, :pady => 5)
            end

            Ttk::TLabel.new(@details) do
                text 'Position: '
                grid(:row => 2, :column => 0, :sticky => 'nsew')
            end

            Ttk::TCombobox.new(@details) do
                textvariable $var4
                values 'Mason Driver Helper Foreman Office'
                state 'readonly'
                grid(:row => 2, :column => 1, :sticky => 'nsew')
            end
			
			Ttk::TLabel.new(@details) do
                text 'Days: '
                grid(:row => 3, :column => 0, :sticky => 'nsew')
            end

            Ttk::TSpinbox.new(@details) do
				textvariable $var5
                to 31
                from 0
                state 'readonly'
                grid(:row => 3, :column => 1, :sticky => 'nsew')
            end
			
			
            @buttonsframe = Ttk::TFrame.new($add_worker_window)
            @buttonsframe.grid(:row => 4, :column => 0)

            @ok = Ttk::TButton.new(@buttonsframe) do
                grid(:row => 0, :column => 0, :sticky => 'nsew')
                command "$add_worker_window.destroy"
            end

            temp = Ttk::TButton.new(@buttonsframe) do
                text 'Exit'
                grid(:row => 0, :column => 1, :sticky => 'nsew')
                command "$add_worker_window.destroy"
            end

            @ok.configure(:text => 'Edit')
            $var2.value = "#{tree.get(tree.selection[0],1).split(', ')[0]} #{tree.get(tree.selection[0],1).split(', ')[1]}"
            $var3.value = tree.get(tree.selection[0],3)
            $var4.value = tree.get(tree.selection[0],2)
			$var5.value = tree.get(tree.selection[0],5)
            item = tree.selection[0]
            @ok.bind('ButtonRelease-1') {edit(tree)}
        end
    end

    def edit(tree)
	    item = tree.selection[0]
        admin.workers[tree.get(item,0).to_i-1].last_name = "#{$var2.value.split[0]}"
        admin.workers[tree.get(item,0).to_i-1].first_name = "#{$var2.value.split[1]}"
        admin.workers[tree.get(item,0).to_i-1].position = $var4.value
        admin.workers[tree.get(item,0).to_i-1].rate = $var3.value.to_i
		admin.workers[tree.get(item,0).to_i-1].days = $var5.value.to_i
        item.configure(:values => [tree.get(item,0), "#{$var2.value.split[0]}, #{$var2.value.split[1]}", $var4.value, $var3.value, "#{$var3.value.to_i*$var5.value.to_i}",  $var5.value])
    end
end

a=TkRoot.new
admin = Admin.new
admin.load("jic.sqlite")
b=View.new(a,admin)
a.mainloop()
admin.save("jic.sqlite")
