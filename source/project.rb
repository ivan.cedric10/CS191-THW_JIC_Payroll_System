class Project
  attr_accessor :title, :client, :date_started, :date_finished, :workers, :id

  
  def initialize(id=0, title="", workers=[] , client="", date_started="", date_finished="")
    @id = id
    @title = title
    @client = client
    @date_started = date_started
    @date_finished = date_finished
    @workers = workers
  end

  def to_s
    return "#{@id} #{@title} #{@date_started} - #{@date_finished}"
  end
end

#a = Project.new
#puts a.to_str
