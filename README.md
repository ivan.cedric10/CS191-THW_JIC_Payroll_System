# JIC PAYROLL SYSTEM: A CS 191 PROJECT

## Contains:
	* SRS
	* Project Proposal
	* PERT Chart
	* FP Tables (Optimistic, Average, Pessimistic)
	* tasks.txt (contains a description of tasks for this project)
	* readme

## To do:
	*edit FP
		*inquiries and files\objects
		*remove files/objects from add/edit/delete worker
		*decrease output hours from Compute Salary
		*increase output for Search Functionality
		*Also, I think we need to increase the Algo for File Recovery
	*edit pert chart
		*expand main tasks into subtasks
		*change Present Prototype end time
	## Please do as ASAP
	